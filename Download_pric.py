import urllib.request
import os
import random
import string
import hashlib
from PIL import Image
from io import BytesIO
import cv2
# pip install opencv-python
# 輸入URL
url = "https://spas.efoxconn.com/user-server/api/user/getKaptchaImg"

# 下載圖片
for num in range(1, 100):
    try:
        response = urllib.request.urlopen(url)
        if response.getcode() == 200:
            image_data = response.read()
            image = Image.open(BytesIO(image_data))
    
            # 保存图像到临时文件
            temp_image_file = "temp_image.jpg"
            image.save(temp_image_file)
    
            # 使用OpenCV打开并显示图像
            img = cv2.imread(temp_image_file)
            cv2.imshow("Image", img)
    
            # 添加小延迟
            cv2.waitKey(10)
    
            # 請求使用者輸入字串
            user_input = input("請輸入字串：")
    
    
            # 关闭OpenCV图像窗口
            cv2.destroyAllWindows()
    
            # 使用MD5 hash計算文件名
            file_extension = ".jpg"  # 指定文件擴展名
            file_name = ".\\image\\" + f"{user_input}_{hashlib.md5(url.encode()).hexdigest()[:32]}{file_extension}"
    
            # 儲存圖片
            image.save(file_name)
    
            print(f"圖片已成功下載並存儲為 {file_name}")
        else:
            print("無法下載圖片")
    except Exception as e:
        print(f"錯誤：{e}")
