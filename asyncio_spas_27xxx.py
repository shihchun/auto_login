﻿import asyncio
from pyppeteer import launch
import cv2
import numpy as np
import base64
import json
import requests
from datetime import datetime, timedelta, date
import os
import pyautogui
import pyperclip # copy string
import subprocess
# 获取今天的日期
today = date.today().strftime('%Y%m%d')
account = '27xxx'
passw = 'password'
# token = 'eyJhbGciOiJIUzI1NiJ9.eyJhcmVhIjoxLCJwcml2aWxlZ2VWYWx1ZSI6MSwicm9sZSI6W3siaWQiOjEsIm5hbWUiOiJFbmdpbmVlciIsImlzQWN0aXZlIjpudWxsLCJjcmVhdG9yIjpudWxsLCJjcmVhdGVkVGltZSI6bnVsbCwidXBkYXRvciI6bnVsbCwidXBkYXRlZFRpbWUiOm51bGx9XSwiYXBpS2V5Ijoic3BhcyIsImdyb3VwSWQiOjI4OSwicm9sZU5hbWUiOlsiRW5naW5lZXIiXSwiaWQiOjUwNzQsInVzZXJOYW1lIjoi6buD5LuV6YieIiwid29ya0lkIjoiMjcxMjkiLCJjb21EZXB0SWQiOjMxOTIsImdyb3VwIjp7ImlkIjoyODksImJ1c2luZXNzdW5pdElkIjo4MTgsInVuaXROYW1lIjoi56CU55m85bmz6Ie6IiwiZGl2aXNpb25JZCI6MzEwLCJkaXZOYW1lIjoi6YGL566X5oqA6KGT576kL0RQRCIsImRlcGFydG1lbnRJZCI6MzczLCJkZXBOYW1lIjoiRldPRCIsInNlY3Rpb25JZCI6NTAxLCJzZWNOYW1lIjoiRlctQTIiLCJmdW5jdGlvbklkIjo4OCwiZnVuY3Rpb25OYW1lIjpudWxsLCJpc0FjdGl2ZSI6dHJ1ZSwiY3JlYXRvciI6MSwiY3JlYXRlZFRpbWUiOnsibW9udGgiOiJTRVBURU1CRVIiLCJkYXlPZldlZWsiOiJNT05EQVkiLCJkYXlPZlllYXIiOjI0OSwibmFubyI6MCwieWVhciI6MjAyMSwibW9udGhWYWx1ZSI6OSwiZGF5T2ZNb250aCI6NiwiaG91ciI6MTYsIm1pbnV0ZSI6NDcsInNlY29uZCI6MzAsImNocm9ub2xvZ3kiOnsiY2FsZW5kYXJUeXBlIjoiaXNvODYwMSIsImlkIjoiSVNPIn19LCJ1cGRhdG9yIjo3NTgsInVwZGF0ZWRUaW1lIjp7Im1vbnRoIjoiSlVMWSIsImRheU9mV2VlayI6IlNBVFVSREFZIiwiZGF5T2ZZZWFyIjoyMTEsIm5hbm8iOjAsInllYXIiOjIwMjIsIm1vbnRoVmFsdWUiOjcsImRheU9mTW9udGgiOjMwLCJob3VyIjoxNCwibWludXRlIjoyNiwic2Vjb25kIjoxMCwiY2hyb25vbG9neSI6eyJjYWxlbmRhclR5cGUiOiJpc284NjAxIiwiaWQiOiJJU08ifX0sImlzQWRkRGVwdCI6ZmFsc2UsImlzRnVuQWN0aXZlIjpudWxsfSwiaWF0IjoxNjk5ODYwODE2LCJleHAiOjE2OTk5MDA0MTZ9.2bFXJMvwEhgrSKxIIePOrGaNz0NeFXgqhGXFPw0HJaY'  # 将 token 放入 "Authorization" 欄位
tokenname = '甘xx'
import pickle
with open('captcha_model.pkl', 'rb') as model_file:
    model = pickle.load(model_file)

def pppstrr(strr, A, separator='\t'):
    strr = strr + f'{A}{separator}'
    return strr


def calculate_previous_workday():
    current_date = datetime.now()
    # 星期一为0，星期日为6
    weekday = current_date.weekday()

    # 如果是星期一，上一个工作日为上周五
    if weekday == 0:
        return current_date - timedelta(days=3)
    else:
        # 否则，上一个工作日为前一天
        return current_date - timedelta(days=1)

def datetime_serializer(obj):
    if isinstance(obj, datetime):
        return obj.strftime('%Y-%m-%d %H:%M:%S')
    raise TypeError(f'Object of type {obj.__class__.__name__} is not JSON serializable')

def workhoursget(token):
    #目標API的URL
    api_url = "https://spas.efoxconn.com/project-server/api/project/report/getWorkHoursData/"

    previous_workday = calculate_previous_workday()

    data = {
        # 在这里添加你的请求数据
        'businessunitId': 818,
        'divisionId': 310,
        'departmentId': 373,
        'sectionId': 501,
        'key': tokenname,
        'pageNum': 1,
        'pageSize': 20,
        'dateFrom': previous_workday.strftime("%Y-%m-%d"),
        'dateTo': previous_workday.strftime("%Y-%m-%d"),
        'roleType': 0,
        'userId': 0
    }
    #print(data)

    # 设置请求头
    headers = {
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'zh-TW,zh;q=0.8',
        'Content-Type': 'application/json;charset=UTF-8',
        'Token': token
    }

    # 设置请求数据
    

    # 转换数据为 JSON 字符串
    #request_data = json.dumps(data)
    request_data = json.dumps(data, default=datetime_serializer)
    response = requests.post(api_url, data=request_data, headers=headers)

    #print(response)

    # 处理响应
    if response.status_code == 200:
        response_data = response.json()
        #print('伺服器回傳的資料:', response_data)
        return response_data
        # 在这里处理伺服器回傳的数据
    else:
        print('POST請求失敗')


def split_image(image, block_size):
    height, width, _ = image.shape
    block_height, block_width = block_size

    # 计算图像可以切分成多少个小块
    num_blocks_horizontal = width // block_width
    num_blocks_vertical = height // block_height

    # 存储切分后的小块图像
    blocks = []

    for i in range(num_blocks_vertical):
        for j in range(num_blocks_horizontal):
            # 计算每个小块的位置
            x = j * block_width
            y = i * block_height

            # 切分小块图像
            block = image[y:y + block_height, x:x + block_width]
            blocks.append(block)

    return blocks

def priblcok(image):
    # 读取图像

    # 指定小块的大小
    block_size = (50, 25)  # 指定小块的高度和宽度

    # 切分图像
    blocks = split_image(image, block_size)

    # 显示切分后的小块图像
    #for i, block in enumerate(blocks):
    #    if 4 <= i <= 7:
    #        cv2.imshow("mainblock", mainblock)
    #        cv2.waitKey(0)
    #        cv2.destroyAllWindows()
    return blocks


async def main():
    # 指定Chromium的路径
    browser = await launch(executablePath="C:\\Program Files\\BraveSoftware\\Brave-Browser\\Application\\brave.exe", headless=False)
    # 创建一个新页面
    page = await browser.newPage()

    # 访问网站
    await page.goto('https://spas.efoxconn.com/')

    logined = False
    
    # 找到并填写输入框
    await page.type('[tabindex="1"]', account)
    await page.type('[tabindex="2"]', passw)

    while not logined:

        # 截取当前页面的验证码图片
        captcha_element = await page.querySelector('#verifyImg')
        if captcha_element:
            captcha_data = await captcha_element.screenshot()
            captcha_image = cv2.imdecode(np.frombuffer(captcha_data, np.uint8), cv2.IMREAD_COLOR)
            captcha_image = cv2.resize(captcha_image, (300, 50))
            #cv2.imshow('Captcha Image', captcha_image)
            cv2.waitKey(20)
            img = priblcok(captcha_image)
            # Loop 已切的圖
            result = ''
            for i, block in enumerate(img):
                if 4 <= i <= 7:
                    #print(filename[i-4])
                    #training_data.append((block, filename[i-4]))
                    # 将图像展平为一维数组
                    input_data = np.array([block.flatten()], dtype=np.float32)

                    # 使用模型进行预测
                    predicted_label = model.predict(input_data)
                    #print(f"    识别结果: {predicted_label}")
                    result+=predicted_label[0]
            print(f"识别: {result}")
            # 输入验证码
            await page.type('[tabindex="3"]', result)
        else:
            print("无法找到验证码图片元素")

        await asyncio.sleep(2)
        # 点击提交按钮
        await page.click('.el-button--primary')

        try:
            # 使用 page.waitForSelector 来等待具有 role="alert" 的 div 元素出现
            await page.waitForSelector('div[role="alert"]', {'timeout': 2000})  # 5000毫秒超时时间
            #print('在特定时间内找到了具有 role="alert" 的 div 元素。')
            await asyncio.sleep(1)
        except:
            print('在特定时间内未找到具有 role="alert" 的 div 元素。')
            await asyncio.sleep(1)
            logined = True

            # 找到所有tr元素
            tr_elements = await page.querySelectorAll('.el-table__row')
            # 只处理第一个tr元素
            first_tr = tr_elements[0]
            if first_tr:
                # 获取第一个tr元素中的所有class="cell"元素
                cell_elements = await first_tr.querySelectorAll('.cell')
                cell = cell_elements[0]
                cell_text = await page.evaluate('(element) => element.textContent', cell)
                print(cell_text)

                column_11_elements = await first_tr.querySelectorAll('.el-table_1_column_11')
                column_11 = column_11_elements[0]

                Continue_elements = await column_11.querySelectorAll('.el-button--success')
                Continue = Continue_elements[0]
                Continue_text = await page.evaluate('(element) => element.textContent', Continue)

                is_disabled = await page.evaluate('(element) => element.classList.contains("is-disabled")', Continue)
                if is_disabled:
                    print("你已經點過了")
                else:
                    print(Continue_text)
                    await Continue.click()
                    print("OK!!")

            #report_elements = await page.querySelectorAll('.el-submenu__title')
            #report = report_elements[1]
            #report_text = await page.evaluate('(element) => element.textContent', report)
            #print(report_text)
            #await report.click()
            #await asyncio.sleep(1)
#
            #report = report_elements[3]
            #report_text = await page.evaluate('(element) => element.textContent', report)
            #print(report_text)
            #await report.click()
#
            #report_elements = await page.querySelectorAll('li[role="menuitem"]')
            #report = report_elements[16]
            #report_text = await page.evaluate('(element) => element.textContent', report)
            #print(report_text)
            #await report.click()
            #await asyncio.sleep(50)
            #await page.goto('https://spas.efoxconn.com/Project_Report/WorkHours_Report')

            #Querybtn_e = await page.querySelectorAll('button[class="el-button el-button--primary"]')
            #Querybtn = Querybtn_e[0]
            #Querybtn_text = await page.evaluate('(element) => element.textContent', Querybtn)
            #print(Querybtn_text)
            #await Querybtn.click()  

            #await asyncio.sleep(5)

            #Exportbtn_e = await page.querySelectorAll('button[class="el-button el-button--success"]')
            #Exportbtn = Exportbtn_e[0]
            #Exportbtn_text = await page.evaluate('(element) => element.textContent', Exportbtn)
            #print(Exportbtn_text)
            #await Exportbtn.click()
            token = await page.evaluate('localStorage.getItem("token")')
            print(token)
            workhours = workhoursget(token)
            #print(workhours)

            data = workhours.get('data', [])
            dataPage = data.get('dataPage', [])
            items = dataPage.get('items', [])
            #print(items)
            # 遍历 items 列表
            for item in items:
                unit = item.get('unit')
                division = item.get('division')
                dept = item.get('dept')
                section = item.get('section')
                userId = item.get('userId')
                name = item.get('name')
                recordWorkHours = item.get('recordWorkHours')
                projectHours = item.get('projectHours')
                nonprojectHours = item.get('nonprojectHours')
                extendHours = item.get('extendHours')
                totalWorkHours = item.get('totalWorkHours')
                missHours = item.get('missHours')
                otapplyHours = item.get('otapplyHours')
                leaveapplyHours = item.get('leaveapplyHours')
                fillCount = item.get('fillCount')
                missRate = item.get('missRate')
            
            #print(unit)
            #print(division)
            #print(dept)
            #print(section)
            #print(userId)
            #print(name)
            #print("1")
            #print(recordWorkHours)
            #print(projectHours)
            #print(nonprojectHours)
            #print(extendHours)
            #print(totalWorkHours)
            #print(missHours)
            #print(otapplyHours)
            #print(leaveapplyHours)
            #print(fillCount)
            #print(missRate+"%")

            sstr = ""
            sstr = pppstrr(sstr, unit)
            sstr = pppstrr(sstr, division)
            sstr = pppstrr(sstr, dept)
            sstr = pppstrr(sstr, section)
            sstr = pppstrr(sstr, userId)
            sstr = pppstrr(sstr, name)
            sstr = pppstrr(sstr, "1")
            sstr = pppstrr(sstr, recordWorkHours)
            sstr = pppstrr(sstr, projectHours)
            sstr = pppstrr(sstr, extendHours)
            sstr = pppstrr(sstr, extendHours)
            sstr = pppstrr(sstr, totalWorkHours)
            sstr = pppstrr(sstr, missHours)
            sstr = pppstrr(sstr, otapplyHours)
            sstr = pppstrr(sstr, leaveapplyHours)
            sstr = pppstrr(sstr, fillCount)
            sstr = pppstrr(sstr, missRate+"%")
            #print(sstr)
            # 使用 pyperclip 將文件路徑複製到剪貼板
            desktop_path = 'D:\\spas_record\\WorkHoursReport_'+today+'_'+account+'.txt'
            # full_path = file_path + file_name
            pyperclip.copy(sstr)
            try:
                # 打开记事本并写入内容
                with open(desktop_path, 'w', encoding='utf-8') as file:
                    file.write(sstr)
                print(f'成功写入记事本！')
                # subprocess.Popen(['notepad.exe', desktop_path])
            except Exception as e:
                print(f'写入失败：{e}')

            # 关闭浏览器
            await asyncio.sleep(2)
            await browser.close()

        # 等待一段时间以便观察结果
        # await asyncio.sleep(3)
# 运行异步事件循环
asyncio.get_event_loop().run_until_complete(main())
