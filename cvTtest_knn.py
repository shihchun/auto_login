import cv2
import os
import numpy as np
# 加载模型
model = cv2.ml.KNearest_load('captcha_model.xml')
current_dir = os.path.dirname(os.path.abspath(__file__))
# 图片目录中包含验证码图片（例如image/xxxx_sha1.jpg）
image_dir = os.path.join(current_dir, 'image_test')

def split_image(image, block_size):
    height, width, _ = image.shape
    block_height, block_width = block_size

    # 计算图像可以切分成多少个小块
    num_blocks_horizontal = width // block_width
    num_blocks_vertical = height // block_height

    # 存储切分后的小块图像
    blocks = []

    for i in range(num_blocks_vertical):
        for j in range(num_blocks_horizontal):
            # 计算每个小块的位置
            x = j * block_width
            y = i * block_height

            # 切分小块图像
            block = image[y:y + block_height, x:x + block_width]
            blocks.append(block)

    return blocks

def priblcok(img_path):
    # 读取图像
    image = cv2.imread(img_path)

    # 指定小块的大小
    block_size = (50, 25)  # 指定小块的高度和宽度

    # 切分图像
    blocks = split_image(image, block_size)

    # 显示切分后的小块图像
    # for i, block in enumerate(blocks):
    #    if 4 <= i <= 7:
    #        cv2.imshow("mainblock", block)
    #        cv2.waitKey(0)
    #        cv2.destroyAllWindows()

    return blocks

for filename in os.listdir(image_dir):
	if filename.endswith('.jpg'):
		img_path = os.path.join(image_dir, filename)
		img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
		# 将图像展平为一维数组
		input_data = np.array([img.flatten()], dtype=np.float32)
		# 使用模型进行预测
		_, results, _, _ = model.findNearest(input_data, k=4)
		# 将结果转换为字符
		predicted_label = ''.join([chr(int(result)) for result in results[0]])
		print(f"识别结果: {predicted_label}")
		
		# img = priblcok(img_path)
		# Loop 已切的圖
		# result = ''
		# for i, block in enumerate(img):
		# 	if 4 <= i <= 7:
		# 		#print(filename[i-4])
		# 		#training_data.append((block, filename[i-4]))
		# 		# 将图像展平为一维数组
		# 		input_data = np.array([block.flatten()], dtype=np.float32)
		# 		_, results, _, _ = model.findNearest(input_data, k=4)

		# 		# 使用模型进行预测
		# 		predicted_label = ''.join([chr(int(result)) for result in results[0]])
		# 		print(f"    识别结果: {predicted_label}")
		# 		result+=predicted_label[0]
		# print(f"    识别: {result}")


