import cv2
import numpy as np
import pickle
import os
# 加载模型
model = cv2.ml.KNearest_load('captcha_model.xml')
# 加载模型
with open('captcha_model.pkl', 'rb') as model_file:
    model = pickle.load(model_file)

def only_bule(img_path):
    # 读取图像
    image = cv2.imread(img_path)
    # 将图像转换为HSV颜色空间
    hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    # 定义蓝色的HSV范围
    lower_blue = np.array([90, 50, 50])  # 低阈值
    upper_blue = np.array([130, 255, 255])  # 高阈值
 
    # 创建掩码，只保留蓝色部分
    blue_mask = cv2.inRange(hsv_image, lower_blue, upper_blue)
    # 使用掩码来提取蓝色部分
    blue_parts = cv2.bitwise_and(image, image, mask=blue_mask)
    return blue_parts

# 加载待识别的验证码图片
# 图片目录中包含验证码图片（例如image/xxxx_sha1.jpg）
current_dir = os.path.dirname(os.path.abspath(__file__))
image_dir = os.path.join(current_dir, 'image_test')
for filename in os.listdir(image_dir):
    if filename.endswith('.jpg'):
        image_path = os.path.join(image_dir, filename)
        print(image_path)
        # img = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
        img = only_bule(image_path)
        # 对图像进行预处理，例如二值化或缩放

        # 将图像展平为一维数组
        input_data = np.array([img.flatten()], dtype=np.float32)

        # 使用模型进行预测
        predicted_label = model.predict(input_data)

        print(f"    识别结果: {predicted_label}")