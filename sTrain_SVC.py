import cv2
import numpy as np
import os
from sklearn.svm import SVC
import pickle
# pip install scikit-learn
# 数据文件夹路径，包含验证码图片
current_dir = os.path.dirname(os.path.abspath(__file__))
# 图像存储目录的相对路径
image_dir = os.path.join(current_dir, 'image')
only_blue=0

def split_image(image, block_size):
    height, width, _ = image.shape
    block_height, block_width = block_size

    # 计算图像可以切分成多少个小块
    num_blocks_horizontal = width // block_width
    num_blocks_vertical = height // block_height

    # 存储切分后的小块图像
    blocks = []

    for i in range(num_blocks_vertical):
        for j in range(num_blocks_horizontal):
            # 计算每个小块的位置
            x = j * block_width
            y = i * block_height

            # 切分小块图像
            block = image[y:y + block_height, x:x + block_width]
            blocks.append(block)

    return blocks

def priblcok(img_path):
    # 读取图像
    image = cv2.imread(img_path)

    # 指定小块的大小
    block_size = (50, 25)  # 指定小块的高度和宽度

    # 切分图像
    blocks = split_image(image, block_size)

    # 显示切分后的小块图像
    #for i, block in enumerate(blocks):
    #    if 4 <= i <= 7:
    #        cv2.imshow("mainblock", mainblock)
    #        cv2.waitKey(0)
    #        cv2.destroyAllWindows()

    return blocks

def only_bule(img_path):
    # 读取图像
    image = cv2.imread(img_path)
    # 将图像转换为HSV颜色空间
    hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    # 定义蓝色的HSV范围
    lower_blue = np.array([90, 50, 50])  # 低阈值
    upper_blue = np.array([130, 255, 255])  # 高阈值
 
    # 创建掩码，只保留蓝色部分
    blue_mask = cv2.inRange(hsv_image, lower_blue, upper_blue)
    # 使用掩码来提取蓝色部分
    blue_parts = cv2.bitwise_and(image, image, mask=blue_mask)
    return blue_parts

# 收集训练数据
training_data = []

# 图片目录中包含验证码图片（例如image/xxxx_sha1.jpg）
image_dir = os.path.join(current_dir, 'image')
for filename in os.listdir(image_dir):
    if filename.endswith('.jpg'):
        img_path = os.path.join(image_dir, filename)
        label = filename.split('_')[0]  # 从文件名中提取标签
        img = priblcok(img_path)
        # Loop 已切的圖
        for i, block in enumerate(img):
            if 4 <= i <= 7:
                #print(filename[i-4])
                training_data.append((block, filename[i-4]))

# 现在，你可以根据需要对图像进行预处理，例如二值化或缩放

# 准备数据
X = np.array([data[0].flatten() for data in training_data], dtype=np.float32)
y = [data[1] for data in training_data]

# 创建一个支持向量机 (SVM) 模型
svm = SVC(kernel='linear')

# 训练模型
svm.fit(X, y)

# 保存模型到文件
with open('captcha_model.pkl', 'wb') as model_file:
    pickle.dump(svm, model_file)

print("模型已训练并保存")