﻿import asyncio
from pyppeteer import launch
import cv2
import numpy as np
import base64
import datetime
import pyautogui
import pyperclip # copy string
# 获取今天的日期
today = datetime.date.today()

# 定义一个函数来计算前一个工作日的日期
def previous_workday(date):
    while True:
        date -= datetime.timedelta(days=1)
        if date.weekday() < 5:  # 判断是否为周一至周五
            return date
# 计算前一个工作日的日期
previous_workday_date = previous_workday(today)
# 将日期格式化为"YYYY-MM-DD"的字符串
previous_workday_str = previous_workday_date.strftime('%Y-%m-%d')
print(previous_workday_str)
current_date = datetime.date.today().strftime('%Y%m%d')
account = '27xxx'
passw = 'password'

import pickle
with open('captcha_model.pkl', 'rb') as model_file:
    model = pickle.load(model_file)

def split_image(image, block_size):
    height, width, _ = image.shape
    block_height, block_width = block_size

    # 计算图像可以切分成多少个小块
    num_blocks_horizontal = width // block_width
    num_blocks_vertical = height // block_height

    # 存储切分后的小块图像
    blocks = []

    for i in range(num_blocks_vertical):
        for j in range(num_blocks_horizontal):
            # 计算每个小块的位置
            x = j * block_width
            y = i * block_height

            # 切分小块图像
            block = image[y:y + block_height, x:x + block_width]
            blocks.append(block)

    return blocks

def priblcok(image):
    # 读取图像

    # 指定小块的大小
    block_size = (50, 25)  # 指定小块的高度和宽度

    # 切分图像
    blocks = split_image(image, block_size)

    # 显示切分后的小块图像
    #for i, block in enumerate(blocks):
    #    if 4 <= i <= 7:
    #        cv2.imshow("mainblock", mainblock)
    #        cv2.waitKey(0)
    #        cv2.destroyAllWindows()
    return blocks

async def main():
    # 指定Chromium的路径
    browser = await launch(executablePath="C:\\Program Files\\BraveSoftware\\Brave-Browser\\Application\\brave.exe", headless=False)
    # 创建一个新页面
    page = await browser.newPage()

    # 访问网站
    await page.goto('https://spas.efoxconn.com/')

    logined = False
    
    # 找到并填写输入框
    await page.type('[tabindex="1"]', account)
    await page.type('[tabindex="2"]', passw)

    while not logined:

        # 截取当前页面的验证码图片
        captcha_element = await page.querySelector('#verifyImg')
        if captcha_element:
            captcha_data = await captcha_element.screenshot()
            captcha_image = cv2.imdecode(np.frombuffer(captcha_data, np.uint8), cv2.IMREAD_COLOR)
            captcha_image = cv2.resize(captcha_image, (300, 50))
            #cv2.imshow('Captcha Image', captcha_image)
            cv2.waitKey(20)
            img = priblcok(captcha_image)
            # Loop 已切的圖
            result = ''
            for i, block in enumerate(img):
                if 4 <= i <= 7:
                    #print(filename[i-4])
                    #training_data.append((block, filename[i-4]))
                    # 将图像展平为一维数组
                    input_data = np.array([block.flatten()], dtype=np.float32)

                    # 使用模型进行预测
                    predicted_label = model.predict(input_data)
                    #print(f"    识别结果: {predicted_label}")
                    result+=predicted_label[0]
            print(f"    识别: {result}")
            # 输入验证码
            await page.type('[tabindex="3"]', result)
        else:
            print("无法找到验证码图片元素")

        await asyncio.sleep(1)
        # 点击提交按钮
        await page.click('.el-button--primary')

        try:
            # 使用 page.waitForSelector 来等待具有 role="alert" 的 div 元素出现
            await page.waitForSelector('div[role="alert"]', {'timeout': 2000})  # 5000毫秒超时时间
            #print('在特定时间内找到了具有 role="alert" 的 div 元素。')
            await asyncio.sleep(1)
        except:
            #print('在特定时间内未找到具有 role="alert" 的 div 元素。')
            #await asyncio.sleep(1)
            logined = True

            # 找到所有tr元素
            tr_elements = await page.querySelectorAll('.el-table__row')
            # 只处理第一个tr元素
            first_tr = tr_elements[0]
            if first_tr:
                # 获取第一个tr元素中的所有class="cell"元素
                cell_elements = await first_tr.querySelectorAll('.cell')
                cell = cell_elements[0]
                cell_text = await page.evaluate('(element) => element.textContent', cell)
                print(cell_text)

                column_11_elements = await first_tr.querySelectorAll('.el-table_1_column_11')
                column_11 = column_11_elements[0]

                Continue_elements = await column_11.querySelectorAll('.el-button--success')
                Continue = Continue_elements[0]
                Continue_text = await page.evaluate('(element) => element.textContent', Continue)

                is_disabled = await page.evaluate('(element) => element.classList.contains("is-disabled")', Continue)
                if is_disabled:
                    print("你已經點過了")
                    # await browser.close()
                    # return 0
                else:
                    print(Continue_text)
                    await Continue.click()
                    print("OK!!")

            await page.goto('https://spas.efoxconn.com/Project_Report/WorkHours_Report')
            await asyncio.sleep(1)

            date_input = await page.querySelector('input[placeholder="開始日期"]')
            if date_input:
                # 使用page.type()来输入日期
                await date_input.type(previous_workday_str)  # 你可以替换为你想要的日期
                await asyncio.sleep(1)
                pyautogui.press('enter')
                await asyncio.sleep(1)
                pass

            date_input = await page.querySelector('input[placeholder="開始日期"]')
            if date_input:
                # 使用page.type()来输入日期
                await date_input.type(previous_workday_str)  # 你可以替换为你想要的日期
                await asyncio.sleep(1)
                pyautogui.press('enter')
            await asyncio.sleep(1)

            date_input = await page.querySelector('input[placeholder="結束日期"]')
            if date_input:
                # 使用page.type()来输入日期
                await date_input.type(previous_workday_str)  # 你可以替换为你想要的日期
                await asyncio.sleep(1)
                pyautogui.press('enter')
            await asyncio.sleep(1)



            # 使用 page.evaluate 执行点击事件
            await page.evaluate('''() => {
                const button = document.querySelector('.el-button--primary');  // 请替换为实际的按钮选择器
                if (button) {
                    button.click();
                }
            }''')


            await asyncio.sleep(2)
            Exportbtn_e = await page.querySelectorAll('button[class="el-button el-button--success"]')
            Exportbtn = Exportbtn_e[0]
            Exportbtn_text = await page.evaluate('(element) => element.textContent', Exportbtn)
            print(Exportbtn_text)
            await Exportbtn.click()
            await asyncio.sleep(2)
            # 使用 pyperclip 將文件路徑複製到剪貼板
            file_name = 'WorkHoursReport_'+current_date+'_'+account+'.xls'
            # full_path = file_path + file_name
            pyperclip.copy(file_name)

            # 使用 pyautogui 模擬按鍵輸入
            pyautogui.hotkey('ctrl', 'v')  # 粘貼
            pyautogui.press('enter')
            await asyncio.sleep(30)

        await browser.close()
        # 等待一段时间以便观察结果
        # await asyncio.sleep(2)
# 运行异步事件循环
asyncio.get_event_loop().run_until_complete(main())
