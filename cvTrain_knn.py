import cv2
import numpy as np
import os

# 收集训练数据
training_data = []
current_dir = os.path.dirname(os.path.abspath(__file__))
# 图片目录中包含验证码图片（例如image/xxxx_sha1.jpg）
image_dir = os.path.join(current_dir, 'image')

def split_image(image, block_size):
    height, width, _ = image.shape
    block_height, block_width = block_size

    # 计算图像可以切分成多少个小块
    num_blocks_horizontal = width // block_width
    num_blocks_vertical = height // block_height

    # 存储切分后的小块图像
    blocks = []

    for i in range(num_blocks_vertical):
        for j in range(num_blocks_horizontal):
            # 计算每个小块的位置
            x = j * block_width
            y = i * block_height

            # 切分小块图像
            block = image[y:y + block_height, x:x + block_width]
            blocks.append(block)

    return blocks

def priblcok(img_path):
    # 读取图像
    image = cv2.imread(img_path)

    # 指定小块的大小
    block_size = (50, 25)  # 指定小块的高度和宽度

    # 切分图像
    blocks = split_image(image, block_size)

    # 显示切分后的小块图像
    #for i, block in enumerate(blocks):
    #    if 4 <= i <= 7:
    #        cv2.imshow("mainblock", mainblock)
    #        cv2.waitKey(0)
    #        cv2.destroyAllWindows()

    return blocks

for filename in os.listdir(image_dir):
    if filename.endswith('.jpg'):
        img_path = os.path.join(image_dir, filename)
        label = filename.split('_')[0]  # 从文件名中提取标签
        img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)  # 以灰度模式读取图像
        training_data.append((img, label))
        # img = priblcok(img_path)
        # Loop 已切的圖
        # print(label)
        # for i, block in enumerate(img):
        #     if 4 <= i <= 7:
        #         #print(filename[i-4])
        #         # print(filename[i-4])
        #         training_data.append((block, filename[i-4]))

# 现在，你可以根据需要对图像进行预处理，例如二值化或缩放

# 创建一个模型（在这里，我们使用最简单的K-Nearest Neighbors模型）
knn = cv2.ml.KNearest_create()

# 准备数据
X = np.array([data[0].flatten() for data in training_data], dtype=np.float32)
y = np.array([list(map(ord, data[1])) for data in training_data], dtype=np.float32)

# 将验证码字符串展平为一维数组
y = y.reshape(-1, 4)

# 训练模型
knn.train(X, cv2.ml.ROW_SAMPLE, y)

# 保存模型到文件
knn.save('captcha_model.xml')

print("模型已训练并保存")
