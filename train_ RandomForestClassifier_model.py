import cv2
import numpy as np
import os
from sklearn.ensemble import RandomForestClassifier
import pickle
# pip install scikit-learn
# 数据文件夹路径，包含验证码图片
current_dir = os.path.dirname(os.path.abspath(__file__))
# 图像存储目录的相对路径
image_dir = os.path.join(current_dir, 'image')
only_blue=True

def only_bule(img_path):
    # 读取图像
    image = cv2.imread(img_path)
    # 将图像转换为HSV颜色空间
    hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    # 定义蓝色的HSV范围
    lower_blue = np.array([90, 50, 50])  # 低阈值
    upper_blue = np.array([130, 255, 255])  # 高阈值
 
    # 创建掩码，只保留蓝色部分
    blue_mask = cv2.inRange(hsv_image, lower_blue, upper_blue)
    # 使用掩码来提取蓝色部分
    blue_parts = cv2.bitwise_and(image, image, mask=blue_mask)
    return blue_parts

# 收集训练数据
training_data = []

# 图片目录中包含验证码图片（例如image/xxxx_sha1.jpg）
image_dir = os.path.join(current_dir, 'image')
for filename in os.listdir(image_dir):
    if filename.endswith('.jpg'):
        img_path = os.path.join(image_dir, filename)
        label = filename.split('_')[0]  # 从文件名中提取标签
        if only_blue==True:
            img = only_bule(img_path)
        else:
            img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)  # 以灰度模式读取图像
        training_data.append((img, label))

# 现在，你可以根据需要对图像进行预处理，例如二值化或缩放

# 准备数据
X = np.array([data[0].flatten() for data in training_data], dtype=np.float32)
y = [data[1] for data in training_data]

# 创建一个随机森林模型
rf_model = RandomForestClassifier(n_estimators=100, random_state=42)

# 训练模型
rf_model.fit(X, y)

# 保存模型到文件
with open('captcha_model.pkl', 'wb') as model_file:
    pickle.dump(rf_model, model_file)

print("随机森林模型已训练并保存")